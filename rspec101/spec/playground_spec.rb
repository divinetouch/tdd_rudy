require_relative '../lib/playground'

# Describe: use to describe the tests
# context: bundle the examples with common context
describe Playground do
  context 'when there are no children' do
    # before do
    #   @playground = Playground.new(0)
    # end

    # Lazy loading
    # use cache value instead of running blog again and again
    let(:playground) { Playground.new(0) }

    it 'is quite boring place' do
      mood = playground.mood
      expect(mood).to eq('boring')
    end

    it 'is empty' do
      expect(playground).to be_empty
    end
  end
end
