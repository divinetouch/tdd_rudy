require 'rails_helper'
require_relative '../support/login_form'

feature 'achievement page' do
  let(:login_form) { LoginForm.new }
  let(:user) { FactoryBot.create(:user) }

  before do
    user = FactoryBot.create(:user)
    login_form.visit_page.login_as(user)
  end
  scenario 'achievement public page', :vcr do
    achievement = FactoryBot.create(:achievement, title: 'Just did it', user: user)
    visit("/achievements/#{achievement.id}")

    expect(page).to have_content('Just did it')

    # craete list
    # achievements = FactoryBot.create_list(:achievement, 3)
    # p achievements
  end

  scenario 'render markdown description', :vcr do
    achievement = FactoryBot.create(:achievement, description: 'this *was* hard', user: user) 
    visit("/achievements/#{achievement.id}")
    expect(page).to have_css("em", text: 'was')
  end
end
