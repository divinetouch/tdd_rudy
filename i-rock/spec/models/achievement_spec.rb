require 'rails_helper'

RSpec.describe Achievement, type: :model do
  describe 'validations' do
    it 'requires title' do
      achievement = Achievement.new(title: '')
      # achievement.valid?
      # expect(achievement.errors[:title]).to include("can't be blank")
      # expect(achievement.errors[:title]).not_to be_empty
      expect(achievement.valid?).to be_falsy
    end

    it { should have_many(:encouragements) }

    it 'requires title to be unique for one user' do
      user = FactoryBot.create(:user)
      first_achievement = FactoryBot.create(:public_achievement, title: 'First Achievement', user: user)
      new_achievement = Achievement.new(title: 'First Achievement', user: user)
      expect(new_achievement.valid?).to be_falsy
    end

    it 'allows different users to have achievements with identical titles' do
      user1 = FactoryBot.create(:user)
      user2 = FactoryBot.create(:user)
      first_achievement = FactoryBot.create(:public_achievement, title: 'First Achievement', user: user1)
      new_achievement = Achievement.new(title: 'First Achievement', user: user2)
      expect(new_achievement.valid?).to be_truthy
    end

    it 'belongs to user' do
      achievement = Achievement.new(title: 'Some title', user: nil)
      expect(achievement.valid?).to be_falsy
    end

    it 'has belongs_to user association' do
      # First Approach
      user = FactoryBot.create(:user)
      achievement = FactoryBot.create(:public_achievement, user: user)
      expect(achievement.user).to eq(user)
      
      # Second Approach
      u = Achievement.reflect_on_association(:user)
      expect(u.macro).to eq(:belongs_to)
    end

		it "converts markdown to html" do
			achievement = Achievement.new(description: 'Awesome **things** I *actually* did')
			expect(achievement.description_html).to include('<strong>things</strong>')
		end

    it 'has silly title' do
      achievment = Achievement.new(title: "New Achievement", user: FactoryBot.create(:user, email: 'test@test.com'))
      expect(achievment.silly_title).to eq('New Achievement by test@test.com')
    end

    it 'only fetches achievements which title starts from provided letter' do
      user = FactoryBot.create(:user)
      achievment1 = FactoryBot.create(:public_achievement, title: 'Read a book', user: user)
      achievment2 = FactoryBot.create(:public_achievement, title: 'Passed an exam', user: user)
      expect(Achievement.by_letter('R')).to eq([achievment1])
    end

    it 'sorts achievements by user emails' do
      albert = FactoryBot.create(:user, email: 'albert@emai.com')
      rob = FactoryBot.create(:user, email: 'rob@emai.com')
      achievment1 = FactoryBot.create(:public_achievement, title: 'Rocked it', user: rob)
      achievment2 = FactoryBot.create(:public_achievement, title: 'Read a book', user: albert)
      expect(Achievement.by_letter('R')).to eq([achievment2, achievment1])
    end
  end
end
