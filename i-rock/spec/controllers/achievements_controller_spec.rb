require 'rails_helper'

describe AchievementsController, type: :controller do

  shared_examples "public access to achievements" do
    let(:user) { FactoryBot.create(:user) }
    # before do
    #   sign_in(user)
    # end
    #
    after do
      sign_out(user)
    end

    describe "GET index" do
      it "renders :index template" do
        get :index
        expect(response).to render_template(:index)
      end

      it "assigns only public achievements to template" do
        public_achievement = FactoryBot.create(:public_achievement, user: user)
        private_achievement = FactoryBot.create(:private_achievement, user: user)
        get :index
        expect(assigns(:achievements)).to match_array([public_achievement])
      end
    end

  end

  describe "guest user" do
    describe "GET new" do
      it "redirects to login page" do
        get :new
        expect(response).to redirect_to(new_user_session_url)
      end
    end

    it_behaves_like "public access to achievements"

  end

  describe "authenticated user" do
    let(:user) { FactoryBot.create(:user) }
    before do
      sign_in(user)
    end

    after do
      sign_out(user)
    end

    # it_behaves_like "public access to achievements"

    describe "GET show" do
      let(:achievement) { FactoryBot.create(:public_achievement, user: user)}

      it "renders :show template" do
        get :show, params: { id: achievement.id }
        expect(response).to render_template(:show)
      end

      it "assigns requested achievement to @achievment" do
        get :show, params: { id: achievement.id }
        expect(assigns(:achievement)).to eq(achievement)
      end
    end

    describe 'GET new' do
      it 'renders :new template' do
        get :new
        expect(response).to render_template(:new)
      end

      it 'assigns new Achievement to @achievement' do
        get :new
        expect(assigns(:achievement)).to be_a_new(Achievement)
      end
    end

    describe "Post create" do
      context "valid data" do 
        it "redirects to achievments#show", :vcr do
          post :create, params: FactoryBot.attributes_for(:public_achievement), as: :json
          expect(response).to redirect_to(achievement_path(assigns[:achievement]))
        end

        it "creates new achievement in database", :vcr do
          expect {
            post :create, params: FactoryBot.attributes_for(:public_achievement), as: :json
          }.to change(Achievement, :count).by(1)
        end
      end
      context "invalid data" do
        let(:invalid_data) { FactoryBot.attributes_for(:public_achievement, title: '') }
        it "renders :new template" do
          post :create, params: {achievement: invalid_data}
          expect(response).to render_template('achievements/new')
        end
        it "doesn't create new achievement in the database" do
          expect {
            post :create, params: {achievement: invalid_data}
          }.to change(Achievement, :count).by(0)
        end
      end
    end

    context "is not the owner of the achievement" do
      let(:achievement) { FactoryBot.create(:public_achievement, user: FactoryBot.create(:user))}
      describe "GET new" do
        it "redirects to achievements page" do
          get :edit, params: { id: achievement.id }
          expect(response).to redirect_to(achievements_path)
        end
      end
      describe "PUT update" do
        let(:valid_data) { FactoryBot.attributes_for(:public_achievement, title: "New Title") }
        it "redirects to achievements page" do
          put :update, params: { id: achievement.id, achievement: valid_data }
          expect(response).to redirect_to(achievements_path)
        end
      end
      describe "DELETE destroy" do
        it "redirects to achievements page" do
          delete :destroy, params: { id: achievement.id }
          expect(response).to redirect_to(achievements_path)
        end
      end
    end

    context "is the owner of the achievement" do
      let(:achievement) { FactoryBot.create(:public_achievement, user: user)}

      describe "GET edit" do

        it "render :edit template" do
          get :edit, params: { id: achievement.id }
          expect(response).to render_template(:edit)
        end

        it "assigns the requested achievement to template" do
          get :edit, params: { id: achievement.id }
          expect(assigns(:achievement)).to eql(achievement)
        end
      end

      describe "PUT update" do
        context "valid data" do
          let(:valid_data) { FactoryBot.attributes_for(:public_achievement, title: "New Title") }

          it "redirects to achievemnts#show" do
            put :update, params: { id: achievement.id, achievement: valid_data }
            expect(response).to redirect_to(:achievement)
          end

          it "updates achievement in the database" do
            put :update, params: { id: achievement.id, achievement: valid_data }
            achievement.reload
            expect(achievement.title).to eql("New Title")
          end
        end

        context "invalid data" do
          let(:invalid_data) { FactoryBot.attributes_for(:public_achievement, title: "", description: "new") }

          it "renders :edit template" do
            put :update, params: { id: achievement.id, achievement: invalid_data }
            expect(response).to render_template(:edit)
          end
          it "doesn't update achievement in the database" do
            put :update, params: { id: achievement.id, achievement: invalid_data }
            achievement.reload
            expect(achievement.description).not_to eql("new")
          end
        end
      end

      describe "DELETE destroy" do

        it "redirects to achievements#index" do
          delete :destroy, params: { id: achievement.id }
          expect(response).to redirect_to(achievements_path)
        end

        it "deletes achievements from database" do
          delete :destroy, params: { id: achievement.id }
          expect(Achievement.exists?(achievement.id)).to be_falsy
        end
      end
    end
  end
end
