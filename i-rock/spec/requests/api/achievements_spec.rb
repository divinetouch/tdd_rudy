require 'rails_helper'

RSpec.describe "Achievements API", type: :request do
  let(:user) {FactoryBot.create(:user)}

  it 'sends public achievements' do
    public_achievement = FactoryBot.create(:public_achievement, title: 'My achievement', user: user)
    public_achievement = FactoryBot.create(:private_achievement, user: user)

    get '/api/achievements', headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }

    expect(response.status).to eq(200)
    json = JSON.parse(response.body)

    expect(json.count).to eq(1)
  end
end
