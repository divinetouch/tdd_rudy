FactoryBot.define do
  factory :achievement do
    sequence(:title) { |n| "Title #{n}"}
    sequence(:description) { |n| "Achievment description #{n}"}  # using this to generate unique description
    features false
    cover_image "some_file.png"

    # sub factory
    factory :public_achievement do
      privacy :public_access
    end
    factory :private_achievement do
      privacy :private_access
    end
  end
end
