FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "email_test#{n}@email.com" }
    password "secret"
  end
end
