class ApiController < ActionController::Base
  protect_from_forgery with: :null_session

  before_action :validate_header

  private

  def validate_header
    if request.headers['CONTENT_TYPE'] != 'application/json'
      render :index, status:400
    end
  end

end
