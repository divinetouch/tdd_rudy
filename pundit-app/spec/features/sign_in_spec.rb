require 'rails_helper'
require_relative '../support/features/session_helpers'

feature 'Sigin in', :devise do
  let(:session_helper) { SessionHelpers.new }
  let(:user) { FactoryBot.create(:user) }
  scenario 'user cannot sign in if not registered' do
    session_helper.signin('person@example.com', 'password')
    expect(page).to have_content('Invalid Email or password')
  end

  scenario 'user can sign in with valid credentials' do
    session_helper.signin(user.email, user.password)
    expect(page).to have_content('Signed in successfully')
  end

  scenario 'user can sign in with invalid email' do
    session_helper.signin('invalid@email.com', user.password)
    expect(page).to have_content('Invalid Email or password')
  end

  scenario 'user can sign in with invalid password' do
    session_helper.signin(user.email, 'invalpass')
    expect(page).to have_content('Invalid Email or password')
  end
end
