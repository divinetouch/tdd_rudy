class SessionHelpers
  include Capybara::DSL

  def signin(email, password)
    visit 'users/sign_in'
    fill_in 'Email', with: email
    fill_in 'Password', with: password
    click_on 'Log in'
  end
end

